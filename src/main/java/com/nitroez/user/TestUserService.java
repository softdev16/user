/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nitroez.user;

/**
 *
 * @author ckitt
 */
public class TestUserService {
    public static void main(String[] args) {
        System.out.println(UserService.getUser());
        UserService.addUser(new User("x", "password"));
        System.out.println(UserService.getUser());
        User updateUser = new User("usery", "password");
        UserService.updateUser(3, updateUser);
        System.out.println(UserService.getUser());
        UserService.delUser(updateUser);
        System.out.println(UserService.getUser());
        UserService.delUser(1);
        System.out.println(UserService.getUser());
    }
}
